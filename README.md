# **Virtual XP Web**
## Trabajo Practico Final: Kubernetes

*Nota: en este instructivo se asume que se crea el alias de la instalación microk8s para utilizar kubectl*

`echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc`

*y se guardan los cambios*

`source ~/.bashrc`


### Contenido del proyecto: 


Ficheros para la implementacion del proyecto, utilizando el metodo deployment:

> pv-volume.yaml

> virtualxp-deployment.yaml

> pv-claim.yaml

> README.md

> delete-deploy.sh

### Crear un Volumen persistente

Esta es la configuracion del archivo del volumen persistente:

> pv-volume.yaml
```
# Configuracion para crear un volumen persistente

apiVersion: v1
kind: PersistentVolume
metadata:
  name: virtualxp-pv-volume
  labels:
    type: local
spec:
  storageClassName: manual
  # Se le Asigna un tamaño de 2GB
  capacity:
    storage: 2Gi
  # Se le asigna un un modo de acceso de ReadWriteOnce, es decir que un solo nodo puede montar el volumen como lectura-escritura
  accessModes:
    - ReadWriteOnce
  # El volumen se puede encontrar en /mnt/data en el nodo del clúster
  hostPath:
    path: "/mnt/data"
```

para para crear el volumen ejecutamos en la consola

`kubectl apply -f pv-volume.yaml`



### Crear el reclamo del volumen persistente

Los pods utilizan el reclamo de volumen persistente para solicitar almacenamiento físico
Creamos el reclamo del volumen persistente que solicita al volumenn al menos 1 GB y que proporciona acceso de lectura y escritura para al menos un nodo

Esta es la configuración del archivo de reclamo del volumen persistente:

> pv-claim.yaml
```
# Configuracion del archivo para reclamar espacio el volumen creado previamente
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: virtualxp-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
```

Para reclamar el espacio del volumen ejecutamos en la consola:

`kubectl apply -f pv-claim.yaml`


### Despliegue de la aplicación

Creamos el archivo de deploy para que utilice el archivo de reclamo de volumen que creamos y ejecutamos anteriormente.

Esta es la configuración del archivo deploy:

> virtualxp-deployment.yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
 # Definir el nombre del deployment
  name: virtualxp-deployment
  namespace: default
  labels:
    app: Virtualxp
spec:
  # El tiempo máximo en segundos para que un deployment progrese antes de que se considere que ha fallado.
  progressDeadlineSeconds: 600
  # número máximo de revisiones que se mantendrán en el historial
  revisionHistoryLimit: 4
  replicas: 2
  # Selector de etiquetas para Pods. Debe coincidir con las etiquetas de la plantilla de pod.
  selector:
    matchLabels:
      app: Virtualxp
  # La estrategia de implementación que se utilizará para reemplazar los pods existentes por otros nuevos.
  strategy:
    rollingUpdate:
        maxSurge: 25%
        maxUnavailable: 25%  
    type: RollingUpdate
  # Describe los pods que se crearán
  template:
    metadata:
      labels:
        app: Virtualxp
    spec:
      volumes:
      # Crea un volumen a partir de un reclamo
      - name: virtualxp-pv-storage
        persistentVolumeClaim:
          claimName: virtualxp-pv-claim
      #se crea el contenedor
      containers:
      #Se descarga la imagen de dockerhub  
      - image: kevmeister/virtualxp:v1
        # Politica de extraccion de imagenes,puede ser Always, Never, IfNotPresent
        # Por defecto es Always, si se aclara el tag :latest o de lo contrario esta IfNotPresent No se puede actualizar.
        imagePullPolicy: Always
        # Nombre del contender especificado como un DNS_LABEL.
        # Cada contenedor en un pod deber tener un unico nombre (DNS_LABEL).
        name: virtualxp
        ports:
        - name: http
          containerPort: 80
        volumeMounts:
        # Punto de montaje interno de los pods 
          - mountPath: "/var/www/html"
            name: virtualxp-pv-storage
```

Para realizar el deploy ejecutamos en la consola:

`kubectl apply -f virtualxp-deployment.yaml`

Luego exponemos la IP del cluster por un puerto aleatorio

`kubectl expose deployment virtualxp-deployment --type=NodePort`

Ahora deberíamos poder ver nuestro servicio ejecutándose en la información del clúster, (de aca tambien podemos obtener el puerto que se asignó, que es aleatorio)

para eso ejecutamos el comando:

`kubectl get services`

que en mi caso mostró:

```
kevin@cloud:~/virtualxp-web-main$ kubectl get services
NAME                   TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes             ClusterIP   10.152.183.1    <none>        443/TCP        19d
virtualxp-deployment   NodePort    10.152.183.58   <none>        80:30150/TCP   3m28s
```


Podemos acceder al servicio utilizando la IP del clúster o como nuestro servicio es de tipo NodePort, también podemos acceder a él en un puerto de la máquina host mediante un navegador a la dirección [http://hostip:puerto/VirtualXP.htm](url) (en mi caso fue [http://192.168.0.27:30150/VirtualXP.htm](url))

Si queremos ver la cantidad de nodos sea la que se configuró (en este caso 3) ingresamos en la consola:

`kubectl get deployment virtualxp-deployment`

con un resultado similar a este:

```
kevin@cloud:~/virtualxp-web-main$ kubectl get deployment virtualxp-deployment
NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
virtualxp-deployment   2/2     2            2           6m17s

```

y con mas detalle:

`kubectl get pods`

con un resultado similar a este:

```
kevin@cloud:~/virtualxp-web-main$ kubectl get pods
NAME                                    READY   STATUS    RESTARTS   AGE
virtualxp-deployment-685f6c746f-4j2mj   1/1     Running   0          7m10s
virtualxp-deployment-685f6c746f-jws4k   1/1     Running   1          7m10s

```

### Escalamiento

Ante la necesidad de escalar los nodos ya sea en mayor o menor cantidad se puede hacer de 2 formas:


A. Editando el archivo **virtualxp-deployment.yaml** presisamente el campo **replicas** por el numero que querramos lo guardamos y luego ejecutamos

`kubectl apply -f virtualxp-deployment.yaml`

B. Ejecutando un comando sin necesidad de editar el archivo (el dato **virtualxp-deployment** corresponde al nombre que definimos en el deployment del archivo **virtualxp-deployment.yaml** )

`kubectl scale deployments/virtualxp-deployment --replicas=10`


### Limpieza

Para eliminar todo lo relacionado con el deployment y el directorio para los volumenes:

`sh delete-deploy.sh`

