#!/bin/bash
microk8s kubectl delete service virtualxp-deployment && \
microk8s kubectl delete deployment virtualxp-deployment && \
microk8s kubectl delete pvc virtualxp-pv-claim && \
microk8s kubectl delete pv virtualxp-pv-volume

rm -r /mnt/data && echo 'Diretorio data borrado'
